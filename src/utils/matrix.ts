// import math from 'mathjs'
export function stringToAsciiArray(str: string): number[] {
  const asciiArray = [];
  for (let i = 0; i < str.length; i++) {
     const charCode = str.charCodeAt(i);
     asciiArray.push(charCode);
  }
  return asciiArray;
}

export function asciiArrayToString(asciiArray: number[]): string {
  let str = "";
  for (let i = 0; i < asciiArray.length; i++) {
     const char = String.fromCharCode(asciiArray[i]);
     str += char;
  }
  return str;
}

export function createMatrix(rows: number, cols: number, defaultValue = ()=>{return 0}): any[][] {
  const matrix = [];
  for (let i = 0; i < rows; i++) {
    const row = [];
    for (let j = 0; j < cols; j++) {
      row.push(defaultValue());
    }
    matrix.push(row);
  }
  return matrix;
}
/**
 *
 * @param str number[]
 * @param grain number
 * @returns Matrix 1 x grain
 */
export function granulate(str: number[], grain: number) {
  const GRAINED = new Array()
  let temp_rray = new Array()

  str.forEach((character,index)=>{
    temp_rray.push(character)
    if (temp_rray.length == grain) {
      GRAINED.push(temp_rray)
      temp_rray = new Array()
    } else if (index == str.length - 1) {
      while (temp_rray.length < grain) {
        temp_rray.push(0)
      }
      GRAINED.push(temp_rray)
    }

  })

  return GRAINED
}
