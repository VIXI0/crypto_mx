// Composables
import { createRouter, createWebHashHistory } from 'vue-router'
import welcome from '@/views/welcome.vue'
import matrix from '@/views/matrix.vue'
const routes = [

      {
        path: '/',
        name: 'welcome',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: welcome,
      },
      {
        path: '/matrix',
        name: 'matrix',
        component: matrix,
      }
    ]



const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
})

export default router
