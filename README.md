# essentials

## Project setup

```
# yarn
yarn

# npm
npm install

# pnpm
pnpm install
```

### Compiles and hot-reloads

```
# yarn
yarn dev

# npm
npm run dev

# pnpm
pnpm dev
```

